const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Usuarios =  require ('../models/Usuarios');

const UsuariosRouter = express.Router();
UsuariosRouter.use(bodyParser.json());

UsuariosRouter.route('/').
    options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Usuarios.find({}).sort({ Nombre_usuario: -1 })
            .then((usuarios) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuarios);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
            Usuarios.create(req.body)
                .then((usuarios) => {
                    console.log('usuarios Created', usuarios);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(usuarios);

                }, (err) => next(err))
                .catch((err) => next(err));
        })
    .put(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion PUT no es soportada para esta ruta');
        })
    .delete(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion DELETE no es soportada para esta ruta');
        });

UsuariosRouter.route('/:userId').
    options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Usuarios.findById(req.params.userId)
            .then((usuarios) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuarios);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion DELETE no es soportada para esta ruta');

        })
    .put(cors.corsWithOptions,
        (req, res, next) => {
            Usuarios.findByIdAndUpdate(req.params.userId, {
                $set: req.body
            }, { new: true })
                .then((usuarios) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(usuarios);
                }, (err) => next(err))
                .catch((err) => next(err));
        })
    .delete(cors.corsWithOptions,
        (req, res, next) => {
            Usuarios.findByIdAndRemove(req.params.userId)
                .then((resp) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(resp);
                }, (err) => next(err))
                .catch((err) => next(err));
        });


module.exports = UsuariosRouter;
