const mongoose = require('mongoose');
const Schemma = mongoose.Schema;

const UsuariosSchema = new Schemma({

    Nombre_usuario:{
        type: String,
        required: true
    },
    Apellido_usuario:{
        type: String,
        required: true
    },
    Cedula_usuario:{
        type: String,
        required: true
    },
    Codigo_usuario:{
        type: String,
        required: true
    }


},{
    timestamps: true
});

var Usuarios = mongoose.model('Usuario', UsuariosSchema);

module.exports = Usuarios;